<% include InnerBanner %>

<div class="row">
    <div class="large-6 medium-6 small-12 columns">
        $Content
        <% include Phone Numbers=$Phones %>
    </div>
    <div class="large-6 medium-6 small-12 columns">
        $Form
    </div>
</div>
